#!/bin/bash

dirname="$(dirname "$0")"

for commit in "$@"; do
    git checkout $commit 1>&2 || exit 1

    echo "COMMIT $commit"
    "$dirname/speed.sh"
    echo

    git checkout - 1>&2 || exit 1
done
