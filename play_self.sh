#!/bin/bash

dirname="$(dirname "$0")"

tc="$1"; shift
ngames="$1"; shift

if [ -z "$tc" ]; then
    tc='10+0.1'
fi

if [ -z "$ngames" ]; then
    ngames=1
fi

cutechess-cli \
    -fcp arg=stderr.1.log arg=./stockfish \
    -scp arg=stderr.2.log arg=./stockfish \
    -both cmd="$dirname/log_err.sh" name=Stockfish dir=. \
        tc="$tc" option.Hash=64 proto=uci \
    -games "$ngames" -concurrency 4
