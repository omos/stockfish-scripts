#!/bin/bash

dirname="$(dirname "$0")"

commit_base="$1"; shift
commit_test="$1"; shift

tmp_dir="$(mktemp -d)"
exe_base="$tmp_dir/base"
exe_test="$tmp_dir/test"

trap "rm -rf '$tmp_dir'" EXIT

function build_commit() {
    local dst="$1"; shift
    local commit="$1"; shift

    git checkout "$commit" || return 1
    make clean && "$dirname/build.sh" || { git checkout -; return 1; }
    git checkout -

    cp ./stockfish "$dst" || return 1
}

build_commit "$exe_base" "$commit_base" || exit 1
build_commit "$exe_test" "$commit_test" || exit 1

meld <(objdump -Cd "$exe_base") <(objdump -Cd "$exe_test")
