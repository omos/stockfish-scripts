#!/bin/bash

dirname="$(dirname "$0")"

if [ $# -eq 0 ]; then
    echo "usage: [SYZYGY_PATH='...'] [BENCH_ARGS='...'] $0 COMMIT..."
    echo
    echo "  Prints bench for each COMMIT. Output is stored in 'bench-COMMIT.log'."
    echo
    echo "  Command-line arguments:"
    echo "    COMMIT...    list of commits to check"
    echo
    echo "  Environment variables:"
    echo "    SYZYGY_PATH  path to syzygy (enables syzygy bench)"
    echo "    BENCH_ARGS   arguments to bench"
    echo
    echo "  Example:"
    echo "    SYZYGY_PATH=/data/syzygy BENCH_ARGS='128 1 22' $0 master my_patch"
    echo
    echo "  TIP: Put 2>/dev/null after the command to hide status output."
fi

for commit in "$@"; do
    git checkout "$commit" 1>&2 || exit 1
    make clean 1>&2 || { git checkout -; exit 1; }
    "$dirname/build.sh" 1>&2 || { git checkout -; exit 1; }
    git checkout - 1>&2 || exit 1
    "$dirname/get_bench.sh" "bench-$commit.log" "$BENCH_ARGS" || exit 1
done
