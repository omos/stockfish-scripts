#!/usr/bin/env python3

if __name__ == '__main__':
    import sys
    import math
    
    n = 0
    total = 0
    total_sq = 0
    for line in sys.stdin:
        nps = int(line.strip())
        
        total += nps
        total_sq += nps * nps
        n += 1
    
    mean = total / n
    stddev = math.sqrt(total_sq / n - mean * mean)
    
    print('Mean:     {0:12.1f} nps'.format(mean))
    print('Std. dev: {0:12.1f} nps'.format(stddev))
