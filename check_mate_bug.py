#!/usr/bin/python3

import io, subprocess

if __name__ == '__main__':
    import sys

    sf = subprocess.Popen([sys.argv[1]], stdin=subprocess.PIPE,
                          stdout=subprocess.PIPE)
    inf = io.TextIOWrapper(sf.stdin, encoding='utf-8', line_buffering=True)
    outf = io.TextIOWrapper(sf.stdout, encoding='utf-8')

    inf.write('setoption name Threads value 4\n')
    inf.write('setoption name Hash value 256\n')

    inf.write('position fen 8/1p2KP2/1p4q1/1Pp5/2P5/N1Pp1k2/3P4/1N6 b - - 76 40\n') # moves g6g5 e7e8 g5e5 e8d7 e5f6 d7e8 f6e6 e8f8 f3e4 f8g7 e6e7 g7g8 e7g5 g8h7 g5f6 h7g8 f6g6 g8f8 e4e5 f8e7 g6e6 e7d8 e6f7 d8c8 e5d6 c8b8 f7e8 b8b7 e8d8 a3c2 d8c7 b7a6 d6d7 c2e3 d7c8 e3d1 c7b7\n')
    inf.write('d\n')
    inf.write('go depth 100\n')
    while True:
        line = outf.readline()
        sys.stdout.write(line)
        if 'bestmove' in line:
            break
    inf.write('quit\n')
    sf.wait()
