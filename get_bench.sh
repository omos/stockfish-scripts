#!/bin/bash

outfile="${1:-/dev/null}"; shift
args="$1"; shift

(
    echo "setoption name SyzygyPath value $SYZYGY_PATH"
    echo "bench $args"
) | \
    ./stockfish 2>&1 | tee "$outfile" | \
        grep 'Nodes searched' | grep -E -o '[0-9]+' || exit 1
