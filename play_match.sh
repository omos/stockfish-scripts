#!/bin/bash

dirname="$(dirname "$0")"

commit_base="$1"; shift
commit_test="$1"; shift

#opts_base="$1"; shift
#opts_test="$1"; shift

ngames="${1:-1}"; shift
nthreads="${1:-1}"; shift
tc="${1:-10+0.1}"; shift
hash="${1:-4}"; shift

tmp_dir="$(mktemp -d)"
exe_base="$tmp_dir/base"
exe_test="$tmp_dir/test"

trap "rm -rf '$tmp_dir'" EXIT

function build_commit() {
    local dst="$1"; shift
    local commit="$1"; shift

    git checkout "$commit" || return 1
    make clean && "$dirname/build.sh" || { git checkout -; return 1; }
    git checkout -

    cp ./stockfish "$dst" || return 1
}

build_commit "$exe_base" "$commit_base" || exit 1
build_commit "$exe_test" "$commit_test" || exit 1

cutechess-cli \
    -fcp arg=stderr.test.log arg="$exe_test" name=Test \
    -scp arg=stderr.base.log arg="$exe_base" name=Base \
    -both cmd="$dirname/log_err.sh" dir=. proto=uci tc="$tc" \
        option.Hash="$hash" option.Threads="$nthreads" \
    -draw 4 5 -resign 4 1000 \
    -games "$ngames" -concurrency $(( $(nproc) / $nthreads ))
