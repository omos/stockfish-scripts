#!/bin/bash

dirname="$(dirname "$0")"

function get_nps() {
    local binary="$1"

    "$binary" bench 2>&1 | grep 'Nodes/second' | grep -o '[0-9]\+'
}

make clean 1>&2 && "$dirname/build.sh" 1>&2 || exit 1

get_nps ./stockfish >/dev/null
for (( i = 0; i < 20; i++ )); do
    get_nps ./stockfish
done | "$dirname/mean.py" || exit 1
